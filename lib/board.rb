class Board
  $SHIP_MARK = :s
  $DESTR_MARK = :x
  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end #self.default_grid

  def initialize(default = Board.default_grid)
    @grid = default
  end #initialize

  #Counts the number of :s's or ship marks on the board
  def count
    grid.map {|row| row.count($SHIP_MARK)}.reduce(:+)
  end #count

  def count_destroy
    grid.map {|row| row.count($DESTR_MARK)}.reduce(:+)
  end #count

  def empty?(pos=nil)
    if pos != nil
      grid[pos[0]][pos[-1]] == nil
    else
      count == 0 && count_destroy == 0
    end
  end #empty?

  def full?
    grid.map {|row| row.count(nil)}.reduce(:+) == 0
  end

  #Places ship with size, position, and orientation (vertical/horizontal)
  def place_ship(size, pos, orientation)
    #Not required by spec
  end #place_ship

  #Place random ship mark on the board
  def place_random_ship
    full? ? (raise "Board is full.") : grid[rand(grid.size)][rand(grid.size)] = $SHIP_MARK
  end

  def won?
    #This line is better but the test declares an empty board as winning
    #count == 0 && !empty?
    count == 0
  end #won?

  def attack?(pos)
    grid[pos[0]][pos[-1]] == $SHIP_MARK
  end #attack?

  def attack(pos)
    grid[pos[0]][pos[-1]] = $DESTR_MARK
  end #attack

  def [](pos)
    grid[pos[0]][pos[-1]]
  end #[]
end #class Board
