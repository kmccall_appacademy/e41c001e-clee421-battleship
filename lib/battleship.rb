class BattleshipGame
  attr_reader :board, :player

  #spec and attr_reader ordering of player & board are different
  def initialize(player, board)
    @board = board
    @player = player
  end #initialize

  def attack(pos)
    board.attack(pos)
  end #attack

  def count
    board.count
  end #count

  def game_over?
    board.won?
  end #game_over?

  def play_turn
    pos = player.get_play
    attack(pos)
  end #play_turn
end #class BattleshipGame
